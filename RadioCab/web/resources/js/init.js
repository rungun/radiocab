(function ($) {
    $(function () {
        // Nav Mobile
        $('.button-collapse').sideNav({'edge': 'left'});

        $('.slider').slider({full_width: true});
    }); // end of document ready
})(jQuery); // end of jQuery name space
