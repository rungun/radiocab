$(document).ready(function () {
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    //$('.modal-trigger').leanModal();
});
function initAutocomplete() {
    // Location
    var latLocation = 36.879469;
    var lngLocation = 30.667646;
    // Features
    var features = [];
    // Direction
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var middle, distance;
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: latLocation, lng: lngLocation},
        zoom: 15,
        mapTypeId: 'roadmap'
    });
    directionsDisplay.setMap(map);

    var infoWindow = new google.maps.InfoWindow();

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            latLocation = position.coords.latitude;
            lngLocation = position.coords.longitude;
            getMarker(latLocation, lngLocation);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(latLocation, lngLocation),
                map: map
            });
            //infoWindow.setPosition(pos);
            //infoWindow.setContent('You');
            map.setCenter(pos);
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }


    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
    }

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            /*
             var icon = {
             url: place.icon,
             size: new google.maps.Size(71, 71),
             origin: new google.maps.Point(0, 0),
             anchor: new google.maps.Point(17, 34),
             scaledSize: new google.maps.Size(25, 25)
             };
             
             
             // Create a marker for each place.
             markers.push(new google.maps.Marker({
             map: map,
             icon: icon,
             title: place.name,
             position: place.geometry.location
             }));
             */

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }

            calcRoute(
                    new google.maps.LatLng(latLocation, lngLocation),
                    new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng())
                    );

            calcDistance({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()});
            getMarker(place.geometry.location.lat(), place.geometry.location.lng());
            //getMarker(latLocation, lngLocation);
            //latLocation = place.geometry.location.lat();
            //lngLocation = place.geometry.location.lng();
            //getMarker(latLocation, lngLocation);
        });
        map.fitBounds(bounds);
    });

    // Icon
    var iconBase = '/RadioCab/faces/javax.faces.resource/images/shapes/';
    var icons = {
        motorbiketrue: {
            icon: iconBase + 'motorcycling.png'
        },
        cartrue: {
            icon: iconBase + 'cabs.png'
        },
        info: {
            icon: iconBase + 'info-i_maps.png'
        },
        motorbikefalse: {
            icon: iconBase + 'motorcyclingOff.png'
        },
        carfalse: {
            icon: iconBase + 'cabsOff.png'
        }
    };

    function addMarker(feature) {

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(feature['latitudeLocation'], feature['longitudeLocation']),
            icon: icons[feature['driverVehicleType']+feature['driverStatus']].icon,
            map: map,
            title: feature['contactPerson']
        });
        console.log(icons[feature['driverVehicleType']+feature['driverStatus']].icon);
        marker.addListener('click', function () {
            $('#city').text(feature['city']);
            $('#contactPerson').text(feature['contactPerson']);
            $('#mobile').text(feature['mobile']);
            $('#experience').text(feature['experience']);
            $('#driverStatus').text(feature['driverStatus']);
            $('#point').text(feature['driverPoint']);
            $('#fee').text((feature['fee'] * distance) / 1000);
            $('#username').text(feature['username']);
            $('#driver-username').val(feature['username']);
            $('#modal1').openModal();
        });
    }

    function getMarker(latLocation, lngLocation) {
        // Get Driver
        $.getJSON("/RadioCab/DriverAreaLocation?latitude=" + latLocation + "&longitude=" + lngLocation, function (data) {
            $.each(data, function (key, val) {
                features.push(val);
                addMarker(val);
            });
        });
    }

    function calcRoute(a, b) {
        var request = {
            origin: a,
            destination: b,
            // Note that Javascript allows us to access the constant
            // using square brackets and a string value as its
            // "property."
            travelMode: google.maps.TravelMode['DRIVING']
        };
        directionsService.route(request, function (response, status) {
            if (status == 'OK') {
                directionsDisplay.setDirections(response);
                //var m = Math.ceil((response.routes[0].overview_path.length) / 2);
                //middle = response.routes[0].overview_path[m];
                middle = response.routes[0].legs[0].steps[1].end_location;
            }
        });
    }

    function calcDistance(latLng) {
        var bounds = new google.maps.LatLngBounds;
        var geocoder = new google.maps.Geocoder;

        var service = new google.maps.DistanceMatrixService;
        service.getDistanceMatrix({
            origins: [{lat: latLocation, lng: lngLocation}],
            destinations: [latLng],
            travelMode: 'DRIVING',
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function (response, status) {
            if (status !== 'OK') {
                alert('Error was: ' + status);
            } else {
                var originList = response.originAddresses;
                var destinationList = response.destinationAddresses;
                var outputDiv = document.getElementById('output');
                outputDiv.innerHTML = '';

                var showGeocodedAddressOnMap = function (asDestination) {

                    return function (results, status) {
                        if (status === 'OK') {
                            map.fitBounds(bounds.extend(results[0].geometry.location));

                        } else {
                            alert('Geocode was not successful due to: ' + status);
                        }
                    };
                };

                for (var i = 0; i < originList.length; i++) {
                    var results = response.rows[i].elements;
                    geocoder.geocode({'address': originList[i]},
                    showGeocodedAddressOnMap(false));
                    for (var j = 0; j < results.length; j++) {
                        geocoder.geocode({'address': destinationList[j]},
                        showGeocodedAddressOnMap(false));
                        outputDiv.innerHTML += originList[i] + ' to ' + destinationList[j] +
                                ': ' + results[j].distance.text + ' in ' +
                                results[j].duration.text + '<br>';
                        $('#from-loc').val(originList[i]);
                        $('#to-loc').val(destinationList[j]);

                        infoWindow.setPosition(middle);
                        infoWindow.setContent(results[j].distance.text + ' <br/> ' +
                                results[j].duration.text);
                        infoWindow.open(map);
                        distance = results[j].distance.value;
                    }
                }
            }
        });


    }


}
