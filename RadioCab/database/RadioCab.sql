create database RadioCabDB
go
use MangaDB
use RadioCabDB
go
--drop database RadioCabDB

create table userAccount(
id int identity(1,1) primary key,
username nvarchar(30) not null unique,
pass nvarchar(100),
email nvarchar(100),
userType nvarchar(30),
isActivate int,
isLogged bit,
)

create table userDriver(
username nvarchar(30) primary key foreign key references userAccount(username) on delete cascade on update cascade,
contactPerson nvarchar(50),
city nvarchar(100),
mobile nvarchar(30),
telephone nvarchar(30),
drivingLicense text,--img name
driverVehicleImage text,--img name
driverVehicleType nvarchar(50), --car / motorbike
experience int,
latitudeLocation float,
longitudeLocation float,
fee float,
driverStatus bit,
dateEnd date,
driverPoint int,
amount int,
)

create table userCompany(
username nvarchar(30) primary key foreign key references userAccount(username) on delete cascade on update cascade,
contactPerson nvarchar(50),
mobile nvarchar(30),
telephone nvarchar(30),
city nvarchar(100),
companyAddress text,
comDescription text,
companyImage text,
dateEnd date,
amount int,
)

create table userAdmin(
username nvarchar(30) primary key foreign key references userAccount(username) on delete cascade on update cascade,
contactPerson nvarchar(50),
phone nvarchar(30),

)
create table userClient(
username nvarchar(30) primary key foreign key references userAccount(username) on delete cascade on update cascade,
contactPerson nvarchar(50),
phone nvarchar(30),
imageProfile text,
)



create table tempUserDriver(
id int identity(1,1) primary key,
confirmKey nvarchar(30),
username nvarchar(30),
pass nvarchar(100),
email nvarchar(100),
userType nvarchar(30),
contactPerson nvarchar(50),
city nvarchar(100),
mobile nvarchar(30),
telephone nvarchar(30),
experience int,
)



create table history(
id int identity(1,1) primary key,
driverId int foreign key references userAccount(id),
clientId int foreign key references userAccount(id),
fromLocation text,
toLocation text,
dateSend date,
checkedStatus int, --0: unread, 1:accepted , 2:declined
rate int,
feedback text
)

create table statistic(
id int identity(1,1) primary key,
driverId int foreign key references userAccount(id) on delete cascade on update cascade,
times int,
inWeek int,
inMonth int,
inYear int,
)

create table forgotPass(
id int identity(1,1) primary key,
username nvarchar(30),
confirmKey nvarchar(30),
)

CREATE TRIGGER RemoveTempAfterRegTrigger
    ON userAccount
    after insert
AS
    DELETE FROM tempUserDriver WHERE id IN(SELECT inserted.id FROM inserted)
GO

/*
select * from dbo.userDriver
select * from userAccount
delete from userAccount where id=1
drop trigger RemoveTempAfterRegTrigger
select * from tempUserDriver
insert into userAccount values ('aomine','123','aomine@gmail','admin',0)
insert into tempUserDriver values (1,'aomine','123','aomine@gmail','driver','Aomine D','city','mobile','telep','image','image',3)
insert into history values(1,5,'Hbt','Gp','2014-1-1')
select * from history
select * from statistic
select * from userCompany
select * from userClient
insert into statistic values(1,9,5,4,2015)
*/
insert into userAccount values ('aomine','123','aomine@gmail','driver',0)
insert into userDriver values('aomine','Aomine D','Toho','123456','12346864','img driving license','img vehicle','motorbike',2,36.879466,3,30.667648,0,CONVERT (date, GETDATE()),0,0)

insert into userAccount values ('shika','123','shika@gmail','driver',0)
insert into userDriver values('shika','Shika D','Jp','7532456','55683','img driving license','img vehicle','motorbike',2,36.883707,30.689216,4,0,CONVERT (date, GETDATE()),0,0)

select * from userDriver u where (SQRT((((u.latitudeLocation - 36.879466) * (u.latitudeLocation - 36.879466)) + ((u.longitudeLocation - 30.689216) * (u.longitudeLocation - 30.689216)))) < 0.01)




insert into userAccount values ('admin','admin','admin@gmail','admin',0)
insert into userAdmin values ('admin','Nguyen Van A','123456789')


insert into userAccount values ('wolf','123','soi@gmail','driver',0)
insert into userDriver values('wolf','Nguyen Viet','HTM','123456','12346864','img driving license','img vehicle','motorbike',2,21.0357497,105.77821,3,0,CONVERT (date, GETDATE()),0,0)

insert into userAccount values('client','123','client@gmail','client',0)


--insert driver
insert into userAccount values ('thuan','123','thuan@gmail','driver',0)
insert into userDriver values('thuan','Thuan N','NT','123456','12346864','img driving license','img vehicle','motorbike',2,21.036191,105.782566,3,0,CONVERT (date, GETDATE()),0,0)

insert into userAccount values ('loc','123','loc@gmail','driver',0)
insert into userDriver values('loc','Loc V','XD','123456','12346864','img driving license','img vehicle','motorbike',2,21.027942,105.775256,3,0,CONVERT (date, GETDATE()),0,0)

insert into userAccount values ('quang','123','quang@gmail','driver',0)
insert into userDriver values('quang','Quang Vo','Ts','123456','12346864','img driving license','img vehicle','car',2,21.036384,105.774795,3,0,CONVERT (date, GETDATE()),0,0)

insert into userAccount values ('duc','123','duc@gmail','driver',0)
insert into userDriver values('duc','Duc N','BV','123456','12346864','img driving license','img vehicle','car',2,21.028219,105.790796,3,0,CONVERT (date, GETDATE()),0,0)

insert into userAccount values ('son','123','son@gmail','driver',0)
insert into userDriver values('son','Son N','Rt','123456','12346864','img driving license','img vehicle','motorbike',2,21.063496,105.788361,3,0,CONVERT (date, GETDATE()),0,0)



--insert company
insert into userAccount values ('gcom','123','gcom@gmail','company',0)
insert into userCompany values ('gcom','Nguyen Com Pa','987656789','12345678','Hanoi','Giai phong','This is Description','image',CONVERT (date, GETDATE()),0)

insert into userAccount values ('rcom','123','gcom@gmail','company',0)
insert into userCompany values ('rcom','Nguyen Com Ra','987656789','12345678','Hanoi','My Dinh','This is Description','image',CONVERT (date, GETDATE()),0)


insert into history values(1,5,'Giai phong','Ho Guom',CONVERT (date, GETDATE()),0,null,'')
insert into history values(1,5,'Ho Tay','Ho Guom',CONVERT (date, GETDATE()),0,null,'')
insert into history values(1,5,'Lac Long Quan','CVA',CONVERT (date, GETDATE()),0,null,'')
insert into history values(1,5,'Giai phong','Pham Hung',CONVERT (date, GETDATE()),0,null,'')