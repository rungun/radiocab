/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shikamaru
 */
@Entity
@Table(name = "tempUserDriver")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TempUserDriver.findAll", query = "SELECT t FROM TempUserDriver t"),
    @NamedQuery(name = "TempUserDriver.findById", query = "SELECT t FROM TempUserDriver t WHERE t.id = :id"),
    @NamedQuery(name = "TempUserDriver.findByConfirmKey", query = "SELECT t FROM TempUserDriver t WHERE t.confirmKey = :confirmKey"),
    @NamedQuery(name = "TempUserDriver.findByUsername", query = "SELECT t FROM TempUserDriver t WHERE t.username = :username"),
    @NamedQuery(name = "TempUserDriver.findByPass", query = "SELECT t FROM TempUserDriver t WHERE t.pass = :pass"),
    @NamedQuery(name = "TempUserDriver.findByEmail", query = "SELECT t FROM TempUserDriver t WHERE t.email = :email"),
    @NamedQuery(name = "TempUserDriver.findByUserType", query = "SELECT t FROM TempUserDriver t WHERE t.userType = :userType"),
    @NamedQuery(name = "TempUserDriver.findByContactPerson", query = "SELECT t FROM TempUserDriver t WHERE t.contactPerson = :contactPerson"),
    @NamedQuery(name = "TempUserDriver.findByCity", query = "SELECT t FROM TempUserDriver t WHERE t.city = :city"),
    @NamedQuery(name = "TempUserDriver.findByMobile", query = "SELECT t FROM TempUserDriver t WHERE t.mobile = :mobile"),
    @NamedQuery(name = "TempUserDriver.findByTelephone", query = "SELECT t FROM TempUserDriver t WHERE t.telephone = :telephone"),
    @NamedQuery(name = "TempUserDriver.findByExperience", query = "SELECT t FROM TempUserDriver t WHERE t.experience = :experience")})
public class TempUserDriver implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Size(max = 30)
    @Column(name = "confirmKey")
    private String confirmKey;
    @Size(max = 30)
    @Column(name = "username")
    private String username;
    @Size(max = 100)
    @Column(name = "pass")
    private String pass;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 30)
    @Column(name = "userType")
    private String userType;
    @Size(max = 50)
    @Column(name = "contactPerson")
    private String contactPerson;
    @Size(max = 100)
    @Column(name = "city")
    private String city;
    @Size(max = 30)
    @Column(name = "mobile")
    private String mobile;
    @Size(max = 30)
    @Column(name = "telephone")
    private String telephone;
    @Column(name = "experience")
    private Integer experience;

    public TempUserDriver() {
    }

    public TempUserDriver(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getConfirmKey() {
        return confirmKey;
    }

    public void setConfirmKey(String confirmKey) {
        this.confirmKey = confirmKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TempUserDriver)) {
            return false;
        }
        TempUserDriver other = (TempUserDriver) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.TempUserDriver[ id=" + id + " ]";
    }
    
}
