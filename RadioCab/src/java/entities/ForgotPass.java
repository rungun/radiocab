/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shikamaru
 */
@Entity
@Table(name = "forgotPass")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ForgotPass.findAll", query = "SELECT f FROM ForgotPass f"),
    @NamedQuery(name = "ForgotPass.findById", query = "SELECT f FROM ForgotPass f WHERE f.id = :id"),
    @NamedQuery(name = "ForgotPass.findByUsername", query = "SELECT f FROM ForgotPass f WHERE f.username = :username"),
    @NamedQuery(name = "ForgotPass.findByConfirmKey", query = "SELECT f FROM ForgotPass f WHERE f.confirmKey = :confirmKey")})
public class ForgotPass implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Size(max = 30)
    @Column(name = "username")
    private String username;
    @Size(max = 30)
    @Column(name = "confirmKey")
    private String confirmKey;

    public ForgotPass() {
    }

    public ForgotPass(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getConfirmKey() {
        return confirmKey;
    }

    public void setConfirmKey(String confirmKey) {
        this.confirmKey = confirmKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ForgotPass)) {
            return false;
        }
        ForgotPass other = (ForgotPass) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ForgotPass[ id=" + id + " ]";
    }
    
}
