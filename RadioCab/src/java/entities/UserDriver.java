/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shikamaru
 */
@Entity
@Table(name = "userDriver")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserDriver.findAll", query = "SELECT u FROM UserDriver u"),
    @NamedQuery(name = "UserDriver.findByUsername", query = "SELECT u FROM UserDriver u WHERE u.username = :username"),
    @NamedQuery(name = "UserDriver.findByContactPerson", query = "SELECT u FROM UserDriver u WHERE u.contactPerson = :contactPerson"),
    @NamedQuery(name = "UserDriver.findByCity", query = "SELECT u FROM UserDriver u WHERE u.city = :city"),
    @NamedQuery(name = "UserDriver.findByMobile", query = "SELECT u FROM UserDriver u WHERE u.mobile = :mobile"),
    @NamedQuery(name = "UserDriver.findByTelephone", query = "SELECT u FROM UserDriver u WHERE u.telephone = :telephone"),
    @NamedQuery(name = "UserDriver.findByDriverVehicleType", query = "SELECT u FROM UserDriver u WHERE u.driverVehicleType = :driverVehicleType"),
    @NamedQuery(name = "UserDriver.findByExperience", query = "SELECT u FROM UserDriver u WHERE u.experience = :experience"),
    @NamedQuery(name = "UserDriver.findByLatitudeLocation", query = "SELECT u FROM UserDriver u WHERE u.latitudeLocation = :latitudeLocation"),
    @NamedQuery(name = "UserDriver.findByLongitudeLocation", query = "SELECT u FROM UserDriver u WHERE u.longitudeLocation = :longitudeLocation"),
    @NamedQuery(name = "UserDriver.findByFee", query = "SELECT u FROM UserDriver u WHERE u.fee = :fee"),
    @NamedQuery(name = "UserDriver.findByDriverStatus", query = "SELECT u FROM UserDriver u WHERE u.driverStatus = :driverStatus"),
    @NamedQuery(name = "UserDriver.findByDateRemaining", query = "SELECT u FROM UserDriver u WHERE u.dateRemaining = :dateRemaining"),
    @NamedQuery(name = "UserDriver.findByDriverPoint", query = "SELECT u FROM UserDriver u WHERE u.driverPoint = :driverPoint"),
    @NamedQuery(name = "UserDriver.findByAmount", query = "SELECT u FROM UserDriver u WHERE u.amount = :amount")})
public class UserDriver implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "username")
    private String username;
    @Size(max = 50)
    @Column(name = "contactPerson")
    private String contactPerson;
    @Size(max = 100)
    @Column(name = "city")
    private String city;
    @Size(max = 30)
    @Column(name = "mobile")
    private String mobile;
    @Size(max = 30)
    @Column(name = "telephone")
    private String telephone;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "drivingLicense")
    private String drivingLicense;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "driverVehicleImage")
    private String driverVehicleImage;
    @Size(max = 50)
    @Column(name = "driverVehicleType")
    private String driverVehicleType;
    @Column(name = "experience")
    private Integer experience;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "latitudeLocation")
    private Double latitudeLocation;
    @Column(name = "longitudeLocation")
    private Double longitudeLocation;
    @Column(name = "fee")
    private Double fee;
    @Column(name = "driverStatus")
    private Boolean driverStatus;
    @Column(name = "dateRemaining")
    private Integer dateRemaining;
    @Column(name = "driverPoint")
    private Integer driverPoint;
    @Column(name = "amount")
    private Integer amount;
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private UserAccount userAccount;

    public UserDriver() {
    }

    public UserDriver(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDrivingLicense() {
        return drivingLicense;
    }

    public void setDrivingLicense(String drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public String getDriverVehicleImage() {
        return driverVehicleImage;
    }

    public void setDriverVehicleImage(String driverVehicleImage) {
        this.driverVehicleImage = driverVehicleImage;
    }

    public String getDriverVehicleType() {
        return driverVehicleType;
    }

    public void setDriverVehicleType(String driverVehicleType) {
        this.driverVehicleType = driverVehicleType;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Double getLatitudeLocation() {
        return latitudeLocation;
    }

    public void setLatitudeLocation(Double latitudeLocation) {
        this.latitudeLocation = latitudeLocation;
    }

    public Double getLongitudeLocation() {
        return longitudeLocation;
    }

    public void setLongitudeLocation(Double longitudeLocation) {
        this.longitudeLocation = longitudeLocation;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Boolean getDriverStatus() {
        return driverStatus;
    }

    public void setDriverStatus(Boolean driverStatus) {
        this.driverStatus = driverStatus;
    }

    public Integer getDateRemaining() {
        return dateRemaining;
    }

    public void setDateRemaining(Integer dateRemaining) {
        this.dateRemaining = dateRemaining;
    }

    public Integer getDriverPoint() {
        return driverPoint;
    }

    public void setDriverPoint(Integer driverPoint) {
        this.driverPoint = driverPoint;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @JsonIgnore
    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (username != null ? username.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserDriver)) {
            return false;
        }
        UserDriver other = (UserDriver) object;
        if ((this.username == null && other.username != null) || (this.username != null && !this.username.equals(other.username))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.UserDriver[ username=" + username + " ]";
    }
    
}
