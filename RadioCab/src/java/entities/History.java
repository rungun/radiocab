/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shikamaru
 */
@Entity
@Table(name = "history")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "History.findAll", query = "SELECT h FROM History h"),
    @NamedQuery(name = "History.findById", query = "SELECT h FROM History h WHERE h.id = :id"),
    @NamedQuery(name = "History.findByDateSend", query = "SELECT h FROM History h WHERE h.dateSend = :dateSend"),
    @NamedQuery(name = "History.findByCheckedStatus", query = "SELECT h FROM History h WHERE h.checkedStatus = :checkedStatus"),
    @NamedQuery(name = "History.findByRate", query = "SELECT h FROM History h WHERE h.rate = :rate")})
public class History implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "fromLocation")
    private String fromLocation;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "toLocation")
    private String toLocation;
    @Column(name = "dateSend")
    @Temporal(TemporalType.DATE)
    private Date dateSend;
    @Column(name = "checkedStatus")
    private Integer checkedStatus;
    @Column(name = "rate")
    private Integer rate;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "feedback")
    private String feedback;
    @JoinColumn(name = "driverId", referencedColumnName = "id")
    @ManyToOne
    private UserAccount driverId;
    @JoinColumn(name = "clientId", referencedColumnName = "id")
    @ManyToOne
    private UserAccount clientId;

    public History() {
    }

    public History(UserAccount clientId, UserAccount driverId, String fromLocation, String toLocation, Date dateSend, Integer checkedStatus ,Integer rate, String feedback) {
        this.fromLocation = fromLocation;
        this.toLocation = toLocation;
        this.dateSend = dateSend;
        this.checkedStatus = checkedStatus;
        this.driverId = driverId;
        this.clientId = clientId;
        this.rate=rate;
        this.feedback=feedback;
    }
    
    public History(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public Date getDateSend() {
        return dateSend;
    }

    public void setDateSend(Date dateSend) {
        this.dateSend = dateSend;
    }

    public Integer getCheckedStatus() {
        return checkedStatus;
    }

    public void setCheckedStatus(Integer checkedStatus) {
        this.checkedStatus = checkedStatus;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public UserAccount getDriverId() {
        return driverId;
    }

    public void setDriverId(UserAccount driverId) {
        this.driverId = driverId;
    }

    public UserAccount getClientId() {
        return clientId;
    }

    public void setClientId(UserAccount clientId) {
        this.clientId = clientId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof History)) {
            return false;
        }
        History other = (History) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.History[ id=" + id + " ]";
    }
    
}
