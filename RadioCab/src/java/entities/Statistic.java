/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shikamaru
 */
@Entity
@Table(name = "statistic")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Statistic.findAll", query = "SELECT s FROM Statistic s"),
    @NamedQuery(name = "Statistic.findById", query = "SELECT s FROM Statistic s WHERE s.id = :id"),
    @NamedQuery(name = "Statistic.findByTimes", query = "SELECT s FROM Statistic s WHERE s.times = :times"),
    @NamedQuery(name = "Statistic.findByInWeek", query = "SELECT s FROM Statistic s WHERE s.inWeek = :inWeek"),
    @NamedQuery(name = "Statistic.findByInMonth", query = "SELECT s FROM Statistic s WHERE s.inMonth = :inMonth"),
    @NamedQuery(name = "Statistic.findByInYear", query = "SELECT s FROM Statistic s WHERE s.inYear = :inYear")})
public class Statistic implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "times")
    private Integer times;
    @Column(name = "inWeek")
    private Integer inWeek;
    @Column(name = "inMonth")
    private Integer inMonth;
    @Column(name = "inYear")
    private Integer inYear;
    @JoinColumn(name = "driverId", referencedColumnName = "id")
    @ManyToOne
    private UserAccount driverId;

    public Statistic() {
    }

    public Statistic(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public Integer getInWeek() {
        return inWeek;
    }

    public void setInWeek(Integer inWeek) {
        this.inWeek = inWeek;
    }

    public Integer getInMonth() {
        return inMonth;
    }

    public void setInMonth(Integer inMonth) {
        this.inMonth = inMonth;
    }

    public Integer getInYear() {
        return inYear;
    }

    public void setInYear(Integer inYear) {
        this.inYear = inYear;
    }

    public UserAccount getDriverId() {
        return driverId;
    }

    public void setDriverId(UserAccount driverId) {
        this.driverId = driverId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Statistic)) {
            return false;
        }
        Statistic other = (Statistic) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Statistic[ id=" + id + " ]";
    }
    
}
