/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shikamaru
 */
@Entity
@Table(name = "userCompany")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserCompany.findAll", query = "SELECT u FROM UserCompany u"),
    @NamedQuery(name = "UserCompany.findByUsername", query = "SELECT u FROM UserCompany u WHERE u.username = :username"),
    @NamedQuery(name = "UserCompany.findByContactPerson", query = "SELECT u FROM UserCompany u WHERE u.contactPerson = :contactPerson"),
    @NamedQuery(name = "UserCompany.findByMobile", query = "SELECT u FROM UserCompany u WHERE u.mobile = :mobile"),
    @NamedQuery(name = "UserCompany.findByTelephone", query = "SELECT u FROM UserCompany u WHERE u.telephone = :telephone"),
    @NamedQuery(name = "UserCompany.findByCity", query = "SELECT u FROM UserCompany u WHERE u.city = :city"),
    @NamedQuery(name = "UserCompany.findByDateRemaining", query = "SELECT u FROM UserCompany u WHERE u.dateRemaining = :dateRemaining"),
    @NamedQuery(name = "UserCompany.findByAmount", query = "SELECT u FROM UserCompany u WHERE u.amount = :amount")})
public class UserCompany implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "username")
    private String username;
    @Size(max = 50)
    @Column(name = "contactPerson")
    private String contactPerson;
    @Size(max = 30)
    @Column(name = "mobile")
    private String mobile;
    @Size(max = 30)
    @Column(name = "telephone")
    private String telephone;
    @Size(max = 100)
    @Column(name = "city")
    private String city;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "companyAddress")
    private String companyAddress;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "comDescription")
    private String comDescription;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "companyImage")
    private String companyImage;
    @Column(name = "dateRemaining")
    private Integer dateRemaining;
    @Column(name = "amount")
    private Integer amount;
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private UserAccount userAccount;

    public UserCompany() {
    }

    public UserCompany(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getComDescription() {
        return comDescription;
    }

    public void setComDescription(String comDescription) {
        this.comDescription = comDescription;
    }

    public String getCompanyImage() {
        return companyImage;
    }

    public void setCompanyImage(String companyImage) {
        this.companyImage = companyImage;
    }

    public Integer getDateRemaining() {
        return dateRemaining;
    }

    public void setDateRemaining(Integer dateRemaining) {
        this.dateRemaining = dateRemaining;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (username != null ? username.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserCompany)) {
            return false;
        }
        UserCompany other = (UserCompany) object;
        if ((this.username == null && other.username != null) || (this.username != null && !this.username.equals(other.username))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.UserCompany[ username=" + username + " ]";
    }
    
}
