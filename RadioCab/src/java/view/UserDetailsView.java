/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import entities.UserAccount;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import sessionbean.UserAccountFacadeLocal;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ViewScoped
public class UserDetailsView {

    @EJB
    private UserAccountFacadeLocal userAccountFacade;

    /**
     * Creates a new instance of UserDetailsView
     */
    public UserDetailsView() {
    }
    private int id;

    private UserAccount user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserAccount getUser() {
        return user;
    }
    
    public void onload() {
        user = userAccountFacade.find(id);
        System.out.println("---" + user.getUsername() + " " + user.getUserType());
    }
}
