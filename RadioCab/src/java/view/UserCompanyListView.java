/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import entities.UserCompany;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import service.UserCompanyService;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ViewScoped
public class UserCompanyListView {

    /**
     * Creates a new instance of UserCompanyListView
     */
    public UserCompanyListView() {
    }
    private List<UserCompany> list;

    private List<UserCompany> filteredUser;

    @ManagedProperty("#{userCompanyService}")
    private UserCompanyService service;

    private UserCompany selectedUserCompany;

    public UserCompany getSelectedUserDriver() {
        return selectedUserCompany;
    }

    public void setSelectedUserDriver(UserCompany selectedUserCompany) {
        this.selectedUserCompany = selectedUserCompany;
    }
    

    public List<UserCompany> getFilteredUser() {
        return filteredUser;
    }

    public void setFilteredUser(List<UserCompany> filteredUser) {
        this.filteredUser = filteredUser;
    }

    

    public void setService(UserCompanyService service) {
        this.service = service;
    }

    public List<UserCompany> getList() {
        return list;
    }

    
    @PostConstruct
    public void init() {
        list=service.getUserCompany();
    }
}
