/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import entities.History;
import entities.UserAccount;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import model.Biz;
import sessionbean.HistoryFacadeLocal;
import sessionbean.UserAccountFacadeLocal;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ViewScoped
public class DriverMessagesBean {
    @EJB
    private HistoryFacadeLocal historyFacade;

    @EJB
    private Biz biz;

    /**
     * Creates a new instance of DriverMessagesBean
     */
    public DriverMessagesBean() {
    }

    List<History> list;
    
//    public void onload(int code) {
//        user = userAccountFacade.find(code);
//        System.out.println("---" + user.getUsername() + " " + user.getUserType());
//    }
    
    @PostConstruct
    public void init(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        int uid = (Integer) session.getAttribute("userid");
        String uType = (String) session.getAttribute("type");
        

        list=biz.driverCheck(uid);
        numberMsg=list.size();
        
    }
    private int numberMsg;

    public int getNumberMsg() {
        return numberMsg;
    }

    public void setNumberMsg(int numberMsg) {
        this.numberMsg = numberMsg;
    }
    
    
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<History> getList() {
        return list;
    }

    public void setList(List<History> list) {
        this.list = list;
    }
    
    public void accept(int id){
        History h=historyFacade.find(id);
        h.setCheckedStatus(1);
        historyFacade.edit(h);
        
        int driverId=Integer.parseInt(this.id);
        list=biz.driverCheck(driverId);
    }
    
    public void decline(int id){
        History h=historyFacade.find(id);
        h.setCheckedStatus(2);
        historyFacade.edit(h);
        int driverId=Integer.parseInt(this.id);
        list=biz.driverCheck(driverId);
    }
}
