/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import model.Biz;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ViewScoped
public class NotifyBean implements Serializable {

    @EJB
    private Biz biz;

    /**
     * Creates a new instance of NotifyBean
     */
    public NotifyBean() {
    }
    @PostConstruct
    public void init(){
        
        
        number=biz.driverCheck(1).size();
    }
    
    
    private int number;
 
    public int getNumber() {
        return number;
    }
 
    public void increment() {
        System.out.println("Print");
//        number=biz.driverCheck(1).size();
    }
    
    
}
