/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import entities.UserAccount;
import entities.UserDriver;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import service.UserDriverService;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ViewScoped
public class UserDriverView implements Serializable{

    /**
     * Creates a new instance of UserView
     */
    public UserDriverView() {
    }
    private List<UserDriver> list;

    private List<UserDriver> filteredUser;

    @ManagedProperty("#{userDriverService}")
    private UserDriverService service;

    private UserDriver selectedUserDriver;

    public UserDriver getSelectedUserDriver() {
        return selectedUserDriver;
    }

    public void setSelectedUserDriver(UserDriver selectedUserDriver) {
        this.selectedUserDriver = selectedUserDriver;
    }
    

    public List<UserDriver> getFilteredUser() {
        return filteredUser;
    }

    public void setFilteredUser(List<UserDriver> filteredUser) {
        this.filteredUser = filteredUser;
    }

    

    public void setService(UserDriverService service) {
        this.service = service;
    }

    public List<UserDriver> getList() {
        return list;
    }

    
    @PostConstruct
    public void init() {
        list=service.getUserDriverList();
    }
    public void searchList(){
        
    }
    
    
}
