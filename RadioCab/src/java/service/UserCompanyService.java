/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entities.UserAccount;
import entities.UserCompany;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import model.Biz;
import sessionbean.UserCompanyFacadeLocal;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ApplicationScoped
public class UserCompanyService {
    @EJB
    private UserCompanyFacadeLocal userCompanyFacade;
    

    /**
     * Creates a new instance of DriverCompanyService
     */
    public UserCompanyService() {
    }
    
    private List<UserCompany> userCompany=new ArrayList<>();

    public List<UserCompany> getUserCompany() {
        return userCompany;
    }
    
    @PostConstruct
    public void init() {
        userCompany=userCompanyFacade.findAll();
    }
}
