/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entities.UserAccount;
import entities.UserDriver;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import model.Biz;
import sessionbean.UserAccountFacadeLocal;
import sessionbean.UserDriverFacadeLocal;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ApplicationScoped
public class UserDriverService implements Serializable{
    @EJB
    private UserDriverFacadeLocal userDriverFacade;
    
    

    /**
     * Creates a new instance of UserService
     */
    public UserDriverService() {
    }
    private List<UserDriver> userDriverList=new ArrayList<>();

    public List<UserDriver> getUserDriverList() {
        return userDriverList;
    }

    
    @PostConstruct
    public void init() {
        userDriverList=userDriverFacade.findAll();
    }
}
