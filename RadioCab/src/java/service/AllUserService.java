/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entities.UserAccount;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import sessionbean.UserAccountFacadeLocal;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ApplicationScoped
public class AllUserService {
    @EJB
    private UserAccountFacadeLocal userAccountFacade;
    
    
    /**
     * Creates a new instance of AllUserService
     */
    public AllUserService() {
    }
    private List<UserAccount> userAccounts=new ArrayList<>();

    public List<UserAccount> getUserAccounts() {
        return userAccounts;
    }
    
    @PostConstruct
    public void init() {
        userAccounts=userAccountFacade.findAll();
    }
}
