/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entities.ForgotPass;
import entities.History;
import entities.History_;
import entities.Statistic;
import entities.Statistic_;
import entities.TempUserDriver;
import entities.UserAccount;
import entities.UserAccount_;
import entities.UserClient;
import entities.UserCompany;
import entities.UserDriver;
import entities.UserDriver_;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Shikamaru
 */
@Stateless
public class Biz {

    @PersistenceContext(unitName = "RadioCabPU")
    private EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }

    public String regisvalidate(String username, String email) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root us = criteriaQuery.from(UserAccount.class);
        criteriaQuery.where(criteriaBuilder.equal(us.get("username"), username));
        Query query = getEntityManager().createQuery(criteriaQuery);
        List<UserAccount> u = query.getResultList();
        Root ust = criteriaQuery.from(TempUserDriver.class);
        criteriaQuery.where(criteriaBuilder.equal(ust.get("username"), username));
        Query queryt = getEntityManager().createQuery(criteriaQuery);
        List<TempUserDriver> ut = queryt.getResultList();
        if (u.isEmpty() && ut.isEmpty()) {
            criteriaQuery.where(criteriaBuilder.equal(ust.get("email"), email));
            Query queryt2 = getEntityManager().createQuery(criteriaQuery);
            List<TempUserDriver> ut2 = queryt2.getResultList();
            criteriaQuery.where(criteriaBuilder.equal(us.get("email"), email));
            Query query2 = getEntityManager().createQuery(criteriaQuery);
            List<UserAccount> u2 = query2.getResultList();
            if (u2.isEmpty() && ut2.isEmpty()) {
                return "success";
            } else {
                return "email exist";
            }
        } else {
            return "username exist";
        }

    }

    public UserAccount checkUser(String username, String password) {

        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root us = criteriaQuery.from(UserAccount.class);
        criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(us.get("username"), username), criteriaBuilder.equal(us.get("pass"), password)));
        Query query = getEntityManager().createQuery(criteriaQuery);
        List<UserAccount> u = query.getResultList();
        if (!u.isEmpty()) {
            UserAccount user = new UserAccount();
            user = u.get(0);
            return user;
        } else {
            return null;
        }
    }

    public boolean register(TempUserDriver user) {
        try {
            System.out.println("Persist User " + user.getUsername());
            em.persist(user);
            sendcode(user.getEmail(), user.getConfirmKey(), user.getUsername(), user.getUserType());
            return true;
        } catch (javax.persistence.PersistenceException ex) {
            System.out.println("Looks like can not persist");
            return false;
        }

    }

    private void sendcode(String email, String key, String username, String userType) {

        String code = " http://localhost:8080/RadioCab/faces/confirmRegister.xhtml?key=" + key + "&name=" + username + "&type=" + userType;
        String msge = "Click this link to enable your account: ";
        String s3 = msge.concat(code);
        Properties p = new Properties();

        p.put("mail.smtp.auth", "true");
        p.put("mail.smtp.starttls.enable", "true");
        p.put("mail.smtp.host", "smtp.gmail.com");
        p.put("mail.smtp.port", 587);
        Session s = Session.getInstance(p,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("runandgunteam@gmail.com", "runandgun");
                    }
                });
        Message msg = new MimeMessage(s);
        try {
            msg.setFrom(new InternetAddress("runandgunteam@gmail.com"));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            msg.setSubject("Confirm email");
            msg.setText(s3);
            Transport.send(msg);
        } catch (MessagingException ex) {
            Logger.getLogger(Biz.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean confirmReg(String key, String username, String type) {
        TempUserDriver user = checkUserRegister(key, username);
        if (user != null) {
            try {
                UserAccount acc = new UserAccount();
                acc.setUsername(user.getUsername());
                acc.setPass(user.getPass());
                acc.setEmail(user.getEmail());
                acc.setUserType(user.getUserType());
                acc.setIsActivate(1);

                Date date = new Date();

                if (type.equals("driver")) {
                    UserDriver driver = new UserDriver();
                    driver.setUsername(user.getUsername());
                    driver.setContactPerson(user.getContactPerson());
                    driver.setCity(user.getCity());
                    driver.setMobile(user.getMobile());
                    driver.setTelephone(user.getTelephone());
                    driver.setDrivingLicense("default.jpg");
                    driver.setDriverVehicleImage("default.jpg");
                    driver.setExperience(user.getExperience());
                    driver.setDriverStatus(false);
                    driver.setDateRemaining(0);
                    driver.setDriverPoint(0);
                    driver.setAmount(0);
                    
                    driver.setUserAccount(acc);
                    acc.setUserDriver(driver);
                } else if (type.equals("company")) {
                    UserCompany company=new UserCompany();
                    company.setUsername(user.getUsername());
                    company.setContactPerson(user.getContactPerson());
                    company.setCity(user.getCity());
                    company.setMobile(user.getMobile());
                    company.setTelephone(user.getTelephone());
                    company.setCompanyAddress("Please update");
                    company.setComDescription("Please update");
                    company.setDateRemaining(0);
                    company.setAmount(0);
                    
                    company.setUserAccount(acc);
                    acc.setUserCompany(company);
                } else if (type.equals("client")) {
                    UserClient client=new UserClient();
                    client.setUsername(user.getUsername());
                    client.setContactPerson(user.getUserType());
                    client.setPhone(user.getMobile());
                    client.setImageProfile("image");
                    
                    client.setUserAccount(acc);
                    acc.setUserClient(client);
                }

                em.persist(acc);
                em.flush();
                return true;
            } catch (javax.persistence.PersistenceException ex) {
                System.out.println("Looks like can not persist");
                return false;
            }
        }
        return false;
    }

    private TempUserDriver checkUserRegister(String key, String username) {

        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root us = criteriaQuery.from(TempUserDriver.class);
        criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(us.get("confirmKey"), key), criteriaBuilder.equal(us.get("username"), username)));
        Query query = getEntityManager().createQuery(criteriaQuery);
        List<TempUserDriver> u = query.getResultList();
        if (!u.isEmpty()) {
            TempUserDriver user = new TempUserDriver();
            user = u.get(0);
            return user;
        } else {
            return null;
        }
    }

//    public List<String[]> queryList(String userType, String city, String mobile, String status) {
//        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
//        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
//        if (userType.equals("driver")) {
//            Root user = criteriaQuery.from(UserDriver.class);
//            criteriaQuery.multiselect(user.get("contactPerson"), user.get("city"), user.get("mobile"), user.get("driverStatus"));
//            List<Predicate> predicates = new ArrayList<>();
//            if (!city.equals("") && city != null) {
//                System.out.println("city is " + city);
//                predicates.add(criteriaBuilder.like(user.get("city"), city));
//            }
//            if (!mobile.equals("") && mobile != null) {
//                System.out.println("mobile is " + mobile);
//                predicates.add(criteriaBuilder.like(user.get("mobile"), "%" + mobile + "%"));
//            }
//            if (!status.equals("") && mobile != null) {
//                System.out.println("mobile is " + status);
//                predicates.add(criteriaBuilder.like(user.get("driverStatus"), status));
//            }
//            criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
//
//            Query query = em.createQuery(criteriaQuery);
//            List<String[]> result = query.getResultList();
//
//            return result;
//        } else if (userType.equals("company")) {
//            Root user = criteriaQuery.from(UserDriver.class);
//            criteriaQuery.multiselect(user.get("contactPerson"), user.get("city"), user.get("mobile"), user.get("driverStatus"));
//            List<Predicate> predicates = new ArrayList<>();
//            if (!city.equals("") && city != null) {
//                System.out.println("city is " + city);
//                predicates.add(criteriaBuilder.like(user.get("city"), city));
//            }
//            if (!mobile.equals("") && mobile != null) {
//                System.out.println("mobile is " + mobile);
//                predicates.add(criteriaBuilder.like(user.get("mobile"), "%" + mobile + "%"));
//            }
//            if (!status.equals("") && mobile != null) {
//                System.out.println("mobile is " + status);
//                predicates.add(criteriaBuilder.like(user.get("driverStatus"), status));
//            }
//            criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
//
//            Query query = em.createQuery(criteriaQuery);
//            List<String[]> result = query.getResultList();
//
//            return result;
//        }
//        return null;
//    }

//    public List<String[]> getUserList() {
//        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
//        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
//        Root user = criteriaQuery.from(UserDriver.class);
//        criteriaQuery.multiselect(user.get("contactPerson"), user.get("city"), user.get("mobile"), user.get("driverStatus"), user.get("driverPoint"));
//        Query query = em.createQuery(criteriaQuery);
//        List<String[]> result = query.getResultList();
//        return result;
//    }



    public List<UserDriver> searchDriverAround(double latitude, double longitude) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root user = criteriaQuery.from(UserDriver.class);
        Join p = user.join(UserDriver_.userAccount);
        List<Predicate> predicates = new ArrayList<>();
        
        predicates.add(criteriaBuilder.lessThan(criteriaBuilder.sqrt(criteriaBuilder.sum(criteriaBuilder.prod(criteriaBuilder.diff(user.get("latitudeLocation"), latitude), criteriaBuilder.diff(user.get("latitudeLocation"), latitude)), criteriaBuilder.prod(criteriaBuilder.diff(user.get("longitudeLocation"), longitude), criteriaBuilder.diff(user.get("longitudeLocation"), longitude)))), 0.05));
        predicates.add(criteriaBuilder.equal(p.get(UserAccount_.isActivate), 0));
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
        Query query = em.createQuery(criteriaQuery);
        List<UserDriver> result = query.getResultList();
        return result;
    }

    public List<Statistic> driverStatistic(int driverId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root stat = criteriaQuery.from(Statistic.class);
        Join p = stat.join(Statistic_.driverId);
        criteriaQuery.where(criteriaBuilder.equal(p.get(UserAccount_.id), driverId));
        criteriaQuery.orderBy(criteriaBuilder.desc(stat.get(Statistic_.inYear)), criteriaBuilder.asc(stat.get(Statistic_.inMonth)));
        Query query = em.createQuery(criteriaQuery).setMaxResults(6);
        List<Statistic> result = query.getResultList();
        for (Statistic s : result) {
            System.out.println("--" + s.getDriverId().getUsername() + "--" + s.getInMonth() + "--" + s.getTimes());
        }

        return result;
    }

    public List<UserAccount> getUserDriverCompany() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root us = criteriaQuery.from(UserAccount.class);
        criteriaQuery.where(criteriaBuilder.or(criteriaBuilder.equal(us.get("userType"), "driver"), criteriaBuilder.equal(us.get("userType"), "company")));
        Query query = getEntityManager().createQuery(criteriaQuery);
        List<UserAccount> result = query.getResultList();

        System.out.println("--Get all users ");
        for (UserAccount u : result) {
            System.out.println("--------------- " + u.getUsername() + "---" + u.getUserType());
        }

        return result;
    }

    public List<History> driverCheck(int driverId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root his = criteriaQuery.from(History.class);
        Join p = his.join(History_.driverId);
        criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(his.get(History_.checkedStatus), 0), criteriaBuilder.equal(p.get(UserAccount_.id), driverId)));
        criteriaQuery.orderBy(criteriaBuilder.desc(his.get(History_.id)));
        Query query = em.createQuery(criteriaQuery);
        List<History> result = query.getResultList();
        for (History s : result) {
            System.out.println("--" + s.getDriverId().getUsername() + "--" + s.getClientId().getUsername() + "--" + s.getId());
        }
        return result;
    }
    
    public List<History> driverHistory(int driverId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root his = criteriaQuery.from(History.class);
        Join p = his.join(History_.driverId);
        criteriaQuery.where( criteriaBuilder.equal(p.get(UserAccount_.id), driverId));
        criteriaQuery.orderBy(criteriaBuilder.desc(his.get(History_.id)));
        Query query = em.createQuery(criteriaQuery);
        List<History> result = query.getResultList();
        for (History s : result) {
            System.out.println("--" + s.getDriverId().getUsername() + "--" + s.getClientId().getUsername() + "--" + s.getId());
        }
        return result;
    }
    public List<History> clientHistory(int clientId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root his = criteriaQuery.from(History.class);
        Join p = his.join(History_.clientId);
        criteriaQuery.where( criteriaBuilder.equal(p.get(UserAccount_.id), clientId));
        criteriaQuery.orderBy(criteriaBuilder.desc(his.get(History_.id)));
        Query query = em.createQuery(criteriaQuery);
        List<History> result = query.getResultList();
        for (History s : result) {
            System.out.println("--" + s.getDriverId().getUsername() + "--" + s.getClientId().getUsername() + "--" + s.getId());
        }
        return result;
    }

    public String getrandomkey() {
        String ketqua = "";
        String hoa = "QWERTYUIOPASDFGHJKLZXCVBNM";
        String so = "01234567890123456789";
        String randomchuoi = hoa.concat(so);
        Random random = new Random();
        try {
            for (int i = 0; i < 10; i++) {
                int temp = random.nextInt(randomchuoi.length());
                ketqua += randomchuoi.charAt(temp);

            }
        } catch (Exception e) {
            System.err.println();
        }
        return ketqua;
    }

    public String encryptcode(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean checkemail(String email) {
        System.out.println("Email in Checkmail: " + email);
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root us = criteriaQuery.from(UserAccount.class);
        criteriaQuery.where(criteriaBuilder.equal(us.get("email"), email));
        Query query = getEntityManager().createQuery(criteriaQuery);
        List<UserAccount> u = query.getResultList();
        if (u.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public void checkStat(UserAccount userDriver) {
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        System.out.println("Month is " + month);

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root stat = criteriaQuery.from(Statistic.class);
        Join p = stat.join(Statistic_.driverId);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(p.get(UserAccount_.id), userDriver.getId()));
        predicates.add(criteriaBuilder.equal(stat.get(Statistic_.inMonth), month));
        predicates.add(criteriaBuilder.equal(stat.get(Statistic_.inYear), year));

        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

        Query query = em.createQuery(criteriaQuery);
        List<Statistic> result = query.getResultList();

        if (!result.isEmpty()) {
            try {
                Statistic s = result.get(0);
                s.setTimes(s.getTimes() + 1);
                em.merge(s);
                em.flush();
            } catch (Exception e) {
                System.out.println("Cannot merge statistic");
            }
        } else {
            try {
                Statistic s = new Statistic();
                s.setDriverId(userDriver);
                s.setInMonth(month);
                s.setInYear(year);
                s.setTimes(1);
                em.persist(s);
                em.flush();
            } catch (Exception e) {
                System.out.println("Cannot create statistic");
            }
        }

//        return null;
    }

    public void sendforgotmail(String key, String username, String email) {
        String code = " http://localhost:8080/RadioCab/faces/getpassword.xhtml?key=" + key + "&name=" + username;
        String msge = "Click vào link sau để đặt lại mật khẩu: ";
        String s3 = msge.concat(code);
        Properties p = new Properties();

        p.put("mail.smtp.auth", "true");
        p.put("mail.smtp.starttls.enable", "true");
        p.put("mail.smtp.host", "smtp.gmail.com");
        p.put("mail.smtp.port", 587);
        Session s = Session.getInstance(p,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("runandgunteam@gmail.com", "runandgun");
                    }
                });
        Message msg = new MimeMessage(s);
        try {
            msg.setFrom(new InternetAddress("runandgunteam@gmail.com"));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            msg.setSubject("Confirm email");
            msg.setText(s3);
            Transport.send(msg);
        } catch (MessagingException ex) {
            Logger.getLogger(Biz.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getusernameequalemail(String email) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root us = criteriaQuery.from(UserAccount.class);
        criteriaQuery.where(criteriaBuilder.equal(us.get("email"), email));
        Query query = getEntityManager().createQuery(criteriaQuery);
        List<UserAccount> u = query.getResultList();
        UserAccount user = new UserAccount();
        user = u.get(0);
        return user.getUsername();

    }

    public boolean insertfaggot(ForgotPass faggot) {

        em.persist(faggot);
        return true;
    }

    public boolean checkforgotpass(String username, String key) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root us = criteriaQuery.from(ForgotPass.class);
        criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(us.get("confirmKey"), key), criteriaBuilder.equal(us.get("username"), username)));
        Query query = getEntityManager().createQuery(criteriaQuery);
        List<ForgotPass> u = query.getResultList();
        if (u.isEmpty()) {
            return false;
        } else {
            ForgotPass f = new ForgotPass();
            f = u.get(0);
            em.remove(f);
            return true;
        }
    }

    public void resetpass(String username, String password) {
        String encryptpass = encryptcode(password);
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root us = criteriaQuery.from(UserAccount.class);
        criteriaQuery.where(criteriaBuilder.equal(us.get("username"), username));
        Query query = getEntityManager().createQuery(criteriaQuery);
        List<UserAccount> u = query.getResultList();
        if (!u.isEmpty()) {
            UserAccount user = new UserAccount();
            user = u.get(0);
            user.setPass(encryptpass);
            em.merge(user);
        } else {
            Root us2 = criteriaQuery.from(TempUserDriver.class);
            criteriaQuery.where(criteriaBuilder.equal(us2.get("username"), username));
            Query query2 = getEntityManager().createQuery(criteriaQuery);
            List<TempUserDriver> u2 = query.getResultList();
            TempUserDriver user2 = new TempUserDriver();
            user2 = u2.get(0);
            user2.setPass(encryptpass);
            em.merge(user2);
        }
    }

    public void clearresetpass(String username) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root us = criteriaQuery.from(ForgotPass.class);
        criteriaQuery.where(criteriaBuilder.equal(us.get("username"), username));
        Query query = getEntityManager().createQuery(criteriaQuery);
        List<ForgotPass> u = query.getResultList();
        if (!u.isEmpty()) {
            ForgotPass user = new ForgotPass();
            user = u.get(0);
            em.remove(user);
        }
    }
    
}
