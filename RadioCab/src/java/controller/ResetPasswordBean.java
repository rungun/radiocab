/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.Biz;

/**
 *
 * @author Administrator
 */
@Named(value = "resetPasswordBean")
@ManagedBean
@RequestScoped
public class ResetPasswordBean implements Serializable {

    /**
     * Creates a new instance of ResetPasswordBean
     */
    
    @EJB
    private Biz biz;

    /**
     * Creates a new instance of ConfirmationBean
     */
    public ResetPasswordBean() {
    }
    private String key;
    private String username;
    private boolean check;
    private String password;
    private String confirmpass;

    public String getConfirmpass() {
        return confirmpass;
    }

    public void setConfirmpass(String confirmpass) {
        this.confirmpass = confirmpass;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void onload() {
        check = biz.checkforgotpass(username, key);
    }

    public String getpassb(){
        biz.resetpass(username, password);
        return "success";
    }
    public void fowardtohome() throws IOException {
        String uri = "index.xhtml";
        FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);
    }
}
