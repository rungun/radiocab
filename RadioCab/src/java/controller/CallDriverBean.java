/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.History;
import entities.UserAccount;
import entities.UserDriver;
import java.io.Serializable;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.Biz;
import sessionbean.HistoryFacadeLocal;
import sessionbean.UserAccountFacadeLocal;
import sessionbean.UserDriverFacadeLocal;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@RequestScoped
public class CallDriverBean implements Serializable {
    @EJB
    private Biz biz;
    @EJB
    private UserAccountFacadeLocal userAccountFacade;
    @EJB
    private HistoryFacadeLocal historyFacade;
    @EJB
    private UserDriverFacadeLocal userDriverFacade;

    /**
     * Creates a new instance of CallDriverBean
     */
    public CallDriverBean() {
    }
    
    
    
    public void callDriver(int clientId){
        String fromLoc = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("from-loc");
        String toLoc = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("to-loc");
        String driverUsername=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("driver-username");
        System.out.println("Called ");
        System.out.println("From :"+fromLoc);
        System.out.println("To: "+toLoc);
        System.out.println("Driver username "+driverUsername);
        UserDriver ud=userDriverFacade.find(driverUsername);
        int driverId=ud.getUserAccount().getId();
        System.out.println("Id: "+driverId);
        
        UserAccount userDriver=userAccountFacade.find(driverId);
        UserAccount userClient=userAccountFacade.find(clientId);
        
        History h=new History(userClient, userDriver, fromLoc, toLoc, new Date(),0,null,"");
        
        historyFacade.create(h);
        biz.checkStat(userDriver);
        
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growl", new FacesMessage("Wait for response", "Call success!"));
    }
    
}
