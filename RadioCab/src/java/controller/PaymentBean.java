/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.UserAccount;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import model.BaokimSCard;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sessionbean.UserAccountFacadeLocal;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@RequestScoped
public class PaymentBean implements Serializable{
    @EJB
    private UserAccountFacadeLocal userAccountFacade;

    /**
     * Creates a new instance of ActivateAccBean
     */
    public PaymentBean() {
    }
    
    private UserAccount user;
    
    
    
    
    
    private String supplier;
    private String seri;
    private String pincode;

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getSeri() {
        return seri;
    }

    public void setSeri(String seri) {
        this.seri = seri;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }
    
    
    @PostConstruct
    public void init(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        int uid=(Integer) session.getAttribute("userid");
        String uType=(String)session.getAttribute("type");
        System.out.println("UserId is yeah: "+uid);
        System.out.println("User type is: "+uType);
        user=userAccountFacade.find(uid);
    }
    
    
    public String commitActive(){
        System.out.println("commitActive: "+supplier +" seri: "+seri+" pin code: "+pincode);
        BaokimSCard bksc = new BaokimSCard();
        String[] rs = bksc.send(supplier, seri, pincode);

        String responseCode = rs[0];
        String responseBody = rs[1];

        //Xá»­ lÃ½ dá»¯ liá»‡u tráº£ vá»� dáº¡ng json
        try {
            JSONParser parser = new JSONParser();
            Object obj;
            obj = parser.parse(responseBody);

            JSONObject jsonObject = (JSONObject) obj;
            if (!responseCode.equals("200")) {
                //TODO: xá»­ lÃ½ khi Báº£o kim tráº£ vá»� lá»—i
                System.out.println("response code: " + responseCode);
                System.out.println("transaction_id: " + jsonObject.get("transaction_id"));
                System.out.println("error message: " + jsonObject.get("errorMessage"));
                
                return "activateError";
            } else {
                //TODO: xá»­ lÃ½ khi Báº£o kim tráº£ vá»� thÃ nh cÃ´ng
                System.out.println("response code: " + responseCode);
                System.out.println("transaction_id: " + jsonObject.get("transaction_id"));
                System.out.println("amount: " + jsonObject.get("amount"));
                
                int amount= (int) jsonObject.get("amount");
                
                String type=user.getUserType();
                if(type.equals("driver")){
                    user.getUserDriver().setAmount(user.getUserDriver().getAmount()+amount);
                    
                }else if(type.equals("company")){
                    user.getUserCompany().setAmount(user.getUserCompany().getAmount()+amount);
                }
                
                userAccountFacade.edit(user);

                
                return "activateSuccess";
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "error";
        }
    }
    
    
    
    
    
}
