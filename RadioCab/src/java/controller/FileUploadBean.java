/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@RequestScoped
public class FileUploadBean implements Serializable {

    /**
     * Creates a new instance of FileUploadBean
     */
    public FileUploadBean() {
    }
    private String name;
    private UploadedFile resume;

    public UploadedFile getResume() {
        return resume;
    }

    public void setResume(UploadedFile resume) {
        this.resume = resume;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public String uploadResume() throws IOException {
//
//        UploadedFile uploadedPhoto = getResume();
//        //System.out.println("Name " + getName());
//        //System.out.println("tmp directory" System.getProperty("java.io.tmpdir"));
//        //System.out.println("File Name " + uploadedPhoto.getFileName());
//        //System.out.println("Size " + uploadedPhoto.getSize());
//        String filePath = "e:/temp/kk/";
//        byte[] bytes = null;
//
//        if (null != uploadedPhoto) {
//            bytes = uploadedPhoto.getContents();
//            String filename = FilenameUtils.getName(uploadedPhoto.getFileName());
//            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath + filename)));
//            stream.write(bytes);
//            stream.close();
//        }
//
//        return "success";
//    }

    /*  The above code is for file upload using simple mode. */
    //This below code is for file upload with advanced mode.
//    public void uploadPhoto(FileUploadEvent e) throws IOException {
//
//        UploadedFile uploadedPhoto = e.getFile();
//
//        String filePath = "e:/temp/kk/";
//        byte[] bytes = null;
//
//        if (null != uploadedPhoto) {
//            bytes = uploadedPhoto.getContents();
//            String filename = FilenameUtils.getName(uploadedPhoto.getFileName());
//            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath + "shika" + filename)));
//            stream.write(bytes);
//            stream.close();
//        }
//
//        FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, "Your Photo (File Name " + uploadedPhoto.getFileName() + " with size " + uploadedPhoto.getSize() + ")  Uploaded Successfully", ""));
//    }

    public void uploadPhoto2(FileUploadEvent e) throws IOException {

        UploadedFile uploadedPhoto = e.getFile();

//        String filePath = "e:/temp/kk/";
        Path folder = Paths.get("/");
        String filename = FilenameUtils.getBaseName(uploadedPhoto.getFileName());
        String extension = FilenameUtils.getExtension(uploadedPhoto.getFileName());
        Path file = Files.createTempFile(folder, filename + "-", "." + extension);

        try (InputStream input = uploadedPhoto.getInputstream()) {
            Files.copy(input, file, StandardCopyOption.REPLACE_EXISTING);
        }

        FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, "Your Photo (File Name " + file + " with size " + uploadedPhoto.getSize() + ")  Uploaded Successfully", ""));
    }
}
