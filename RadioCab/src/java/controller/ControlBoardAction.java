/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.Statistic;
import entities.UserAccount;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import model.Biz;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import sessionbean.UserAccountFacadeLocal;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@SessionScoped
public class ControlBoardAction implements Serializable {

    @EJB
    private Biz biz;

    /**
     * Creates a new instance of ControlBoardAction
     */
    public ControlBoardAction() {
    }
    @EJB
    private UserAccountFacadeLocal userAccountFacade;

    private UserAccount user;

    public UserAccount getUser() {
        return user;
    }

    @PostConstruct
    public void init() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        int uid = (Integer) session.getAttribute("userid");
        String uType = (String) session.getAttribute("type");

        listStatistic = biz.driverStatistic(uid);
        createLineModels();
        user = userAccountFacade.find(uid);
    }

    public void onload(int code) {
//        listStatistic=biz.driverStatistic(code);
//        createLineModels();
//        user = userAccountFacade.find(code);
//        System.out.println("---" + user.getUsername() + " " + user.getUserType());
    }

    public void updateStatus() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (user.getUserDriver().getDriverStatus() == true) {
            String latString = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("lati");
            String longString = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("long");
            double lat = Double.parseDouble(latString);
            double lng = Double.parseDouble(longString);
            user.getUserDriver().setLatitudeLocation(lat);
            user.getUserDriver().setLongitudeLocation(lng);
            this.userAccountFacade.edit(user);
            context.addMessage("growl", new FacesMessage("Status on", "Online!"));
        } else {
            this.userAccountFacade.edit(user);
            context.addMessage("growl", new FacesMessage("Status off", "Offline!"));
        }

        System.out.println("--" + user.getUserDriver().getDriverStatus());
    }

    public void charge() {
        FacesContext context = FacesContext.getCurrentInstance();
        String type = user.getUserType();
        if (type.equals("driver")) {
            if (user.getUserDriver().getAmount() >= 10000) {
                user.setIsActivate(0);
                user.getUserDriver().setDateRemaining(user.getUserDriver().getDateRemaining() + 30);
                user.getUserDriver().setAmount(user.getUserDriver().getAmount()-10000);
                
                userAccountFacade.edit(user);
                context.addMessage("growl", new FacesMessage("Charge successful", "Have a good day!"));
            }else{
                context.addMessage("growl", new FacesMessage("Charge failed", "You don't have enough money!"));
            }

        } else if (type.equals("company")) {
            if(user.getUserCompany().getAmount()>=10000){
                user.setIsActivate(0);
                user.getUserCompany().setDateRemaining(user.getUserCompany().getDateRemaining() + 30);
                user.getUserCompany().setAmount(user.getUserCompany().getAmount()-10000);
                
                userAccountFacade.edit(user);
                context.addMessage("growl", new FacesMessage("Charge successful", "Have a good day!"));
            }else{
                context.addMessage("growl", new FacesMessage("Charge failed", "You don't have enough money!"));
            }

        }
    }

    List<Statistic> listStatistic;

    public List<Statistic> getListStatistic() {
        return listStatistic;
    }

    public void setListStatistic(List<Statistic> listStatistic) {
        this.listStatistic = listStatistic;
    }

    private LineChartModel lineModel1;

    public LineChartModel getLineModel1() {
        return lineModel1;
    }

    public void setLineModel1(LineChartModel lineModel1) {
        this.lineModel1 = lineModel1;
    }

    private void createLineModels() {
        lineModel1 = initLinearModel();
        lineModel1.setTitle("Chart");
//        lineModel1.setLegendPosition("e");
        lineModel1.setShowPointLabels(true);
        lineModel1.getAxes().put(AxisType.X, new CategoryAxis("Month"));

        Axis yAxis = lineModel1.getAxis(AxisType.Y);
        yAxis.setLabel("Times");
        yAxis.setMin(0);
//        yAxis.setMax(50);
    }

    private LineChartModel initLinearModel() {
        System.out.println("------------------------------------init linear");
        LineChartModel model = new LineChartModel();

        LineChartSeries series1 = new LineChartSeries();
//        series1.setLabel("Series 1");
//        List<Statistic> list = biz.driverStatistic(1);
        if (listStatistic.isEmpty() || listStatistic == null) {
            for (int i = 1; i <= 12; i++) {
                series1.set(i, 0);
            }
        } else {
            for (Statistic s : listStatistic) {
                series1.set(s.getInMonth(), s.getTimes());
            }
        }
        model.addSeries(series1);

        return model;
    }
}
