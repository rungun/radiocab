/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.UserAccount;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import service.AllUserService;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ViewScoped
public class PageAdminManagedBean implements Serializable{

    /**
     * Creates a new instance of PageAdminManagedBean
     */
    public PageAdminManagedBean() {
    }
    @PostConstruct
    public void init(){
        userAccList=service.getUserAccounts();
    }
    private List<UserAccount> userAccList;
    
    private List<UserAccount> filteredUser;

    @ManagedProperty("#{allUserService}")
    private AllUserService service;
    
    private UserAccount selectedUserAcc;

    public List<UserAccount> getUserAccList() {
        return userAccList;
    }
    
    public UserAccount getSelectedUserAcc() {
        return selectedUserAcc;
    }
    
    public void setSelectedUserAcc(UserAccount selectedUserAcc) {
        this.selectedUserAcc = selectedUserAcc;
    }

    public List<UserAccount> getFilteredUser() {
        return filteredUser;
    }

    public void setFilteredUser(List<UserAccount> filteredUser) {
        this.filteredUser = filteredUser;
    }

    public void setService(AllUserService service) {
        this.service = service;
    }
    
    
    

    
    
}
