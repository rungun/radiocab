/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Biz;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@RequestScoped
public class ConfirmationBean implements Serializable {

    @EJB
    private Biz biz;

    /**
     * Creates a new instance of ConfirmationBean
     */
    public ConfirmationBean() {
    }
    private String key;
    private String username;
    private String type;

    private boolean check;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void onload() {
        check = biz.confirmReg(key, username,type);
    }
}
