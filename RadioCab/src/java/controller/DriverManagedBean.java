/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.TempUserDriver;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import model.Biz;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@RequestScoped
public class DriverManagedBean implements Serializable{

    @EJB
    private Biz biz;

    /**
     * Creates a new instance of driverManagedBean
     */
    public DriverManagedBean() {
    }
    @NotNull(message = "trường này không được để trống")
    @Pattern(regexp = "^[A-Za-z0-9_ ]*$", message = "không được chứa ký tự đặc biệt")
    private String fullname;
    @NotNull(message = "trường này không được để trống")
    @Pattern(regexp = "^[A-Za-z0-9]*$", message = "không được chứa ký tự đặc biệt")
    @Size(min = 6, max = 20, message = "hãy nhập tối thiểu 6 ký tự và tối đa 20 ký tự")
    private String username;
    @Pattern(regexp = "^[A-Za-z0-9]*$", message = "không được chứa ký tự đặc biệt")
    @NotNull(message = "trường này không được để trống")
    private String password;
    @Size(min = 6, max = 20, message = "mật khẩu phải có độ dài ít nhất là 6 ký tự")
    @NotNull(message = "trường này không được để trống")
    private String confirmPass;
    @Pattern(regexp = "^[0-9]*$", message = "hãy nhập vào số điện thoại")
    @NotNull(message = "trường này không được để trống")
    private String mobilePhone;
    @Pattern(regexp = ".+@.+\\.[a-z]+", message = "hãy nhập vào email chính xác")
    @NotNull(message = "trường này không được để trống")
    private String email;
    private int experience;
    private String city;
    

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPass() {
        return confirmPass;
    }

    public void setConfirmPass(String confirmPass) {
        this.confirmPass = confirmPass;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void registerDriver() {
        System.out.println("Register driver method");
        String cfkey = biz.getrandomkey();
        TempUserDriver user = new TempUserDriver();
        user.setConfirmKey(cfkey);
        user.setContactPerson(fullname);
        user.setUsername(username);
        String encryptpassword = biz.encryptcode(password);
        user.setPass(encryptpassword);
        user.setEmail(email);
        user.setUserType("driver");
        user.setCity(city);
        user.setMobile(mobilePhone);
        user.setTelephone(mobilePhone);
        user.setExperience(experience);
        boolean reg = biz.register(user);

    }

    public String foward() {
        String a = biz.regisvalidate(username, email);
        if (a.equals("success")) {
            registerDriver();
            return "success";
        } else if (a.equals("email exist")) {
            FacesMessage message = new FacesMessage("Email đã tồn tại");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(email, message);
            return "driverRegister";
        } else {
            FacesMessage message = new FacesMessage("Username đã tồn tại");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(username, message);
            return "driverRegister";
        }
    }

}
