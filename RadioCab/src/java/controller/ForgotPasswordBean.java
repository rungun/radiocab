/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.ForgotPass;
import entities.TempUserDriver;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Biz;

/**
 *
 * @author Administrator
 */
@Named(value = "forgotPasswordBean")
@ManagedBean
@RequestScoped
public class ForgotPasswordBean implements Serializable {

    @EJB
    private Biz biz;
    @PersistenceContext(unitName = "RadioCabPU")
    private EntityManager em;
    /**
     * Creates a new instance of ForgotPasswordBean
     */
    public ForgotPasswordBean() {
    }
    private String email;
    private String username;
    private String key;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String checking() {
        System.out.println("Checking");
        boolean a = biz.checkemail(email);
        if (a) {
            username = biz.getusernameequalemail(email);
            biz.clearresetpass(username);
            System.out.println("username "+username);
            key = biz.getrandomkey();
            biz.sendforgotmail(key, username, email);
            ForgotPass faggot = new ForgotPass();
            faggot.setUsername(username);
            faggot.setConfirmKey(key);
            biz.insertfaggot(faggot);
            return "success";
        } else {
            FacesMessage message = new FacesMessage("Email chưa được đăng ký");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(email, message);
            return "forgotpassword";
        }
    }

}
