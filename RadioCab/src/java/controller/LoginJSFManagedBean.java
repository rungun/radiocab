/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.UserAccount;
import authentication.SessionAccout;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;
import javax.annotation.security.*;
import javax.faces.context.FacesContext;
import model.Biz;
import org.primefaces.context.RequestContext;

/**
 *
 * @author PhuongThuan
 */
@ManagedBean
@SessionScoped
@DeclareRoles({"Admin", "User"})
public class LoginJSFManagedBean implements Serializable{

    @EJB
    private Biz biz;

    public boolean isLogged = false;

    private UserAccount acc = new UserAccount();

    public UserAccount getAcc() {
        return acc;
    }

    public void setAcc(UserAccount acc) {
        this.acc = acc;
    }

    public LoginJSFManagedBean() {
    }


//Login command Button in pageadmin:
    public String loginButton() {
//        String encryptpassword = biz.encryptcode(acc.getPass());
        String encryptpassword=acc.getPass();
        UserAccount suser = biz.checkUser(acc.getUsername(), encryptpassword);
        if (suser != null && (suser.getIsActivate() == 0 || suser.getIsActivate()==1)) {
            HttpSession session = SessionAccout.getSession();
            session.setAttribute("username", suser.getUsername());
            session.setAttribute("type", suser.getUserType());
            session.setAttribute("userid", suser.getId());
            session.setAttribute("isActive", suser.getIsActivate());
            isLogged = true;
            System.out.println(suser.getIsActivate());
            return "index?faces-redirect=true";
        } else if(suser != null && suser.getIsActivate()==3){
            FacesMessage message = new FacesMessage("Tắt máy học bài đi cháu");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(acc.getUsername(), message);
            return "";
        }else{
            FacesMessage message = new FacesMessage("Username hoặc mật khẩu không đúng");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(acc.getUsername(), message);
            return "login";
        }
    }

//Log out command Button:
    public String logout() {
        HttpSession session = SessionAccout.getSession();
        session.invalidate();
        return "index?faces-redirect=true";
    }

    

}
