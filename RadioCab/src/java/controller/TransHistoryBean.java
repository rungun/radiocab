/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.History;
import entities.UserAccount;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import model.Biz;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RateEvent;
import org.primefaces.event.SelectEvent;
import sessionbean.HistoryFacadeLocal;
import sessionbean.UserAccountFacade;
import sessionbean.UserAccountFacadeLocal;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ViewScoped
public class TransHistoryBean implements Serializable {
    @EJB
    private Biz biz;
    @EJB
    private HistoryFacadeLocal historyFacade;

    @EJB
    private UserAccountFacadeLocal userAccountFacade;

    /**
     * Creates a new instance of TransHistoryBean
     */
    public TransHistoryBean() {
    }

    private UserAccount user;

    public UserAccount getUser() {
        return user;
    }

    private List<History> hisList;

    public List<History> getHisList() {
        return hisList;
    }

    public void setHisList(List<History> hisList) {
        this.hisList = hisList;
    }
    
    
    @PostConstruct
    public void init(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        int uid=(Integer) session.getAttribute("userid");
        String uType=(String)session.getAttribute("type");
        System.out.println("UserId is yeah: "+uid);
        System.out.println("User type is: "+uType);
        
        if(uType.equals("driver")){
            hisList=biz.driverHistory(uid);
        }else if(uType.equals("client")){
            hisList=biz.clientHistory(uid);
        }
        
    }
    
    
    public void onload(int code) {
        user = userAccountFacade.find(code);

    }
    private History selectedHis;


    public History getSelectedHis() {
        return selectedHis;
    }

    public void setSelectedHis(History selectedHis) {
        this.selectedHis = selectedHis;
    }

    public void onrate(RateEvent rateEvent) {
//        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Rate Event", "You rated:" + ((Integer) rateEvent.getRating()).intValue());
//        FacesContext.getCurrentInstance().addMessage(null, message);

        int rate = ((Integer) rateEvent.getRating()).intValue();
        System.out.println("On rate clicked: " + rate);
//        System.out.println("Client id "+user.);
        System.out.println("Selected id: " + selectedHis.getId());

//        History h=historyFacade.find(selectedHis.getId());
//        h.setRate(rate);
//        historyFacade.edit(h);
        
        selectedHis.setRate(rate);
        
    }

    public void updateFeedback(){
        
        System.out.println("Selected his updateFeedback: "+selectedHis.getRate());
        System.out.println("Feedback :" +selectedHis.getFeedback());
        
        historyFacade.edit(selectedHis);
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.execute("PF('dialog').hide()");
    }

}
