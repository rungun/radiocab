/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.UserAccount;
import entities.UserDriver;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import model.Biz;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import sessionbean.UserAccountFacadeLocal;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@SessionScoped
public class UserDetails implements Serializable{
    @EJB
    private Biz biz;

    @EJB
    private UserAccountFacadeLocal userAccountFacade;

    /**
     * Creates a new instance of UserDriverDetails
     */
    public UserDetails() {
    }
    private int id;

    private UserAccount user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserAccount getUser() {
        return user;
    }

    public void onload(int code) {
        user = userAccountFacade.find(code);
        System.out.println("---" + user.getUsername() + " " + user.getUserType());
    }



    public String actionUpdate() {
//        boolean b = biz.checkemail(user.getEmail());
//        if (!b){
        this.userAccountFacade.edit(this.user);
        return "profileUser?faces-redirect=true";
//        }
//        else {
//            return "profileUserUpdate";
//        }
    }

    public String buttonEdit() {
        return "profileUserUpdate?faces-redirect=true";
    }


    public void uploadLicensePhoto(FileUploadEvent e) throws IOException {

        UploadedFile uploadedPhoto = e.getFile();

        Path folder = Paths.get("/Program Files/glassfish-4.1/glassfish/domains/domain1/var/webapp/images/");
        String filename = FilenameUtils.getBaseName(uploadedPhoto.getFileName());
        String extension = FilenameUtils.getExtension(uploadedPhoto.getFileName());
        Path file = Files.createTempFile(folder, filename + "-", "." + extension);
        
        user.getUserDriver().setDrivingLicense(file.getFileName().toString());
        System.out.println("-in up license photo: "+file.getFileName().toString());
        try (InputStream input = uploadedPhoto.getInputstream()) {
            Files.copy(input, file, StandardCopyOption.REPLACE_EXISTING);
        }

        FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, "Your Photo (File Name " + file + " with size " + uploadedPhoto.getSize() + ")  Uploaded Successfully", ""));
    }
    
    
    public void uploadVehiclePhoto(FileUploadEvent e) throws IOException {

        UploadedFile uploadedPhoto = e.getFile();

        Path folder = Paths.get("/Program Files/glassfish-4.1/glassfish/domains/domain1/var/webapp/images/");
        String filename = FilenameUtils.getBaseName(uploadedPhoto.getFileName());
        String extension = FilenameUtils.getExtension(uploadedPhoto.getFileName());
        Path file = Files.createTempFile(folder, filename + "-", "." + extension);
        
        user.getUserDriver().setDriverVehicleImage(file.getFileName().toString());
        System.out.println("-in up vehicle photo: "+file.getFileName().toString());
        try (InputStream input = uploadedPhoto.getInputstream()) {
            Files.copy(input, file, StandardCopyOption.REPLACE_EXISTING);
        }

        FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, "Your Photo (File Name " + file + " with size " + uploadedPhoto.getSize() + ")  Uploaded Successfully", ""));
    }
    
    
    
    
    
    public void test(){
//        user.getUserDriver().setDrivingLicense(licensePhoto);
        System.out.println("Dr lice "+user.getUserDriver().getDrivingLicense());
//        System.out.println("Image "+licensePhoto);
//        System.out.println("Vehicle "+vehiclePhoto);
    }
}
