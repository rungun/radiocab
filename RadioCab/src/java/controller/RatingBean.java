/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.RateEvent;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ViewScoped
public class RatingBean implements Serializable {

    /**
     * Creates a new instance of RatingBean
     */
    public RatingBean() {
    }
    public void onrate(RateEvent rateEvent) {
//        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Rate Event", "You rated:" + ((Integer) rateEvent.getRating()).intValue());
//        FacesContext.getCurrentInstance().addMessage(null, message);
        
        int rate=((Integer) rateEvent.getRating()).intValue();
        System.out.println("On rate clicked: "+rate);
        System.out.println("Client id "+clientId);
    }
    
    private int clientId;

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    
}
