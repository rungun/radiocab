/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.UserAccount;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import sessionbean.UserAccountFacadeLocal;
import javax.annotation.security.*;

/**
 *
 * @author PhuongThuan
 */
@ManagedBean
@SessionScoped
public class CRUDJSFManagedBean implements Serializable {

    @EJB
    private UserAccountFacadeLocal tbAccountFacade;

    private UserAccount acc = new UserAccount();

    public UserAccount getAcc() {
        return acc;
    }

    public void setAcc(UserAccount acc) {
        this.acc = acc;
    }

    public CRUDJSFManagedBean() {
    }
    
//Action UPDATE:
    public String buttonUpdate(UserAccount acc) {
        this.acc = acc;
        return "update?faces-redirect=true";
    }

    public String actionUpdate() {
        this.tbAccountFacade.edit(this.acc);
        return "pageadmin?faces-redirect=true";
    }
//====================================================

//Action DELETE:
    public void buttonDelete(UserAccount acc) {
        this.tbAccountFacade.remove(acc);
    }

//Action ADD USER:
    public String buttonAdd() {
        this.tbAccountFacade.create(this.acc);
        return "pageadmin?faces-redirect=true";
    }
//Action BlockUser:
    public String buttonBlock(UserAccount acc) {
        if (acc.getIsActivate() == 0) {
            acc.setIsActivate(3);
            this.tbAccountFacade.edit(acc);
            return "pageadmin?faces-redirect=true";
        } else {
            acc.setIsActivate(0);
            this.tbAccountFacade.edit(acc);
            return "pageadmin?faces-redirect=true";
        }
    }
}
