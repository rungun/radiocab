/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.Statistic;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import model.Biz;
import org.primefaces.event.RateEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author Shikamaru
 */
@ManagedBean
@ViewScoped
public class TestBean implements Serializable {

    @EJB
    private Biz biz;

    /**
     * Creates a new instance of TestBean
     */
    public TestBean() {
    }

    @PostConstruct
    public void init() {
//        System.out.println("From postConstruct");
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        this.id = facesContext.getExternalContext().getRequestParameterMap().get("id");
//        System.out.println("Id is: "+id);
    }

    public void test() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        int uid=(Integer) session.getAttribute("userid");
        System.out.println("UserId is yeah: "+uid);
//        biz.driverHistory(1);
    }

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private int number;

    public int getNumber() {
        return number;
    }

    public void increment() {
        System.out.println("Print");
//        number=biz.driverCheck(1).size();
    }

}
