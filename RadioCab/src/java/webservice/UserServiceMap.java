/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import entities.UserCompany;
import entities.UserDriver;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Biz;
import sessionbean.UserCompanyFacadeLocal;
import sessionbean.UserDriverFacadeLocal;

/**
 *
 * @author Shikamaru
 */
@WebServlet(name = "UserServiceMap", urlPatterns = {"/UserServiceMap"})
public class UserServiceMap extends HttpServlet {

    @EJB
    private UserCompanyFacadeLocal userCompanyFacade;
    @EJB
    private UserDriverFacadeLocal userDriverFacade;
    @EJB
    private Biz biz;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AllUserService</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AllUserService at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<UserDriver> driverList = userDriverFacade.findAll();
        List<UserCompany> compList = userCompanyFacade.findAll();
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        String arrayToJsonDr = mapper.writeValueAsString(driverList);
        String arrayToJsonCom = mapper.writeValueAsString(compList);
        System.out.println(arrayToJsonDr);
        System.out.println(arrayToJsonCom);

        String s3 = new String("");
        arrayToJsonDr = arrayToJsonDr.substring(arrayToJsonDr.indexOf("[") + 1, arrayToJsonDr.indexOf("]"));
        arrayToJsonCom = arrayToJsonCom.substring(arrayToJsonCom.indexOf("[") + 1, arrayToJsonCom.indexOf("]"));
        s3 = "[" + arrayToJsonDr + "," + arrayToJsonCom + "]";

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(s3);

        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
