/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entities.UserDriver;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Shikamaru
 */
@Local
public interface UserDriverFacadeLocal {

    void create(UserDriver userDriver);

    void edit(UserDriver userDriver);

    void remove(UserDriver userDriver);

    UserDriver find(Object id);

    List<UserDriver> findAll();

    List<UserDriver> findRange(int[] range);

    int count();
    
}
