/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entities.ForgotPass;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Shikamaru
 */
@Stateless
public class ForgotPassFacade extends AbstractFacade<ForgotPass> implements ForgotPassFacadeLocal {
    @PersistenceContext(unitName = "RadioCabPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ForgotPassFacade() {
        super(ForgotPass.class);
    }
    
}
