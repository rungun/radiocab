/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entities.TempUserDriver;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Shikamaru
 */
@Local
public interface TempUserDriverFacadeLocal {

    void create(TempUserDriver tempUserDriver);

    void edit(TempUserDriver tempUserDriver);

    void remove(TempUserDriver tempUserDriver);

    TempUserDriver find(Object id);

    List<TempUserDriver> findAll();

    List<TempUserDriver> findRange(int[] range);

    int count();
    
}
