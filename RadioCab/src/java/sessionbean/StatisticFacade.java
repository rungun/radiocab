/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entities.Statistic;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Shikamaru
 */
@Stateless
public class StatisticFacade extends AbstractFacade<Statistic> implements StatisticFacadeLocal {
    @PersistenceContext(unitName = "RadioCabPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StatisticFacade() {
        super(Statistic.class);
    }
    
}
