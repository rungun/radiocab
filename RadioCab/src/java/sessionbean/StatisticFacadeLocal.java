/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entities.Statistic;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Shikamaru
 */
@Local
public interface StatisticFacadeLocal {

    void create(Statistic statistic);

    void edit(Statistic statistic);

    void remove(Statistic statistic);

    Statistic find(Object id);

    List<Statistic> findAll();

    List<Statistic> findRange(int[] range);

    int count();
    
}
