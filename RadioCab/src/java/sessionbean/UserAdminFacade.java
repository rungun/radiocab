/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entities.UserAdmin;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Shikamaru
 */
@Stateless
public class UserAdminFacade extends AbstractFacade<UserAdmin> implements UserAdminFacadeLocal {
    @PersistenceContext(unitName = "RadioCabPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserAdminFacade() {
        super(UserAdmin.class);
    }
    
}
