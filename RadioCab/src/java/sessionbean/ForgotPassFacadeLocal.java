/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entities.ForgotPass;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Shikamaru
 */
@Local
public interface ForgotPassFacadeLocal {

    void create(ForgotPass forgotPass);

    void edit(ForgotPass forgotPass);

    void remove(ForgotPass forgotPass);

    ForgotPass find(Object id);

    List<ForgotPass> findAll();

    List<ForgotPass> findRange(int[] range);

    int count();
    
}
