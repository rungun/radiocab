/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entities.UserCompany;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Shikamaru
 */
@Local
public interface UserCompanyFacadeLocal {

    void create(UserCompany userCompany);

    void edit(UserCompany userCompany);

    void remove(UserCompany userCompany);

    UserCompany find(Object id);

    List<UserCompany> findAll();

    List<UserCompany> findRange(int[] range);

    int count();
    
}
